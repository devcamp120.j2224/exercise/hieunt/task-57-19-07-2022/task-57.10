package com.task5710.absclass.model;

public class Zebra extends Animal {
    private boolean is_wild;
    public Zebra(int age,
    String gender, boolean is_wild) {
        super(age, gender);
        this.is_wild = is_wild;
    }
    public boolean isIs_Wild() {
        return is_wild;
    }
    public void setIs_Wild(boolean is_wild) {
        this.is_wild = is_wild;
    }
    @Override
    public boolean isMammal() {
        return true;
    }
    @Override
    public void mate() {
        System.out.println("Zebra mates");
    }
    public void run() {
        System.out.println("Zebra runs");
    }
}