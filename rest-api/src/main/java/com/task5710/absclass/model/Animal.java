package com.task5710.absclass.model;

public abstract class Animal {
    private int age;
    private String gender;
    public abstract boolean isMammal();
    public void mate() {
        System.out.println("Animal mates");
    }    
    public Animal(int age, String gender) {
        this.age = age;
        this.gender = gender;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
}
