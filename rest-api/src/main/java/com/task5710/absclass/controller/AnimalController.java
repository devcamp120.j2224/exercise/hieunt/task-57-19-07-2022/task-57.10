package com.task5710.absclass.controller;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.*;
import com.task5710.absclass.model.*;

@CrossOrigin
@RestController
public class AnimalController {
    @GetMapping("/listAnimal")
    public ArrayList<Animal> getAnimalList() {
        ArrayList<Animal> animalList = new ArrayList<Animal>();
        Animal animal1 = new Duck(2, "male", "yellow");
        Animal animal2 = new Fish(1, "female", 3, true);
        Animal animal3 = new Zebra(3, "male", true);
        animalList.add(animal1);
        animalList.add(animal2);
        animalList.add(animal3);
        return animalList;
    }
}
